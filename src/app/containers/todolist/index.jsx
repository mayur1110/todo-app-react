import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import * as TodoActions from "app/actions";
import TodoList from "app/components/todolist";
import { TodoListContainerProto } from "app/prototypes";

///////////////

/**
 * class respresenting TodoList Container
 *
 * @class
 */
class TodoListContainer extends React.Component {
  static propTypes = TodoListContainerProto;

  render() {
    return <TodoList todos={this.props.list} {...this.props.actions} />;
  }
}

const mapStateToProps = state => {
  return { list: state.todos };
};
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(TodoActions, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoListContainer);
