import TodoListContainer from "./index";
import configureMockStore from "redux-mock-store";
import TodoList from "app/components/todolist";
import { Provider } from "react-redux";
import * as React from "react";
import { mount } from "enzyme";

describe("TodoList Container", () => {
  const createMockStore = configureMockStore();

  let mockStore;
  let wrapper;

  beforeEach(() => {
    mockStore = createMockStore({
      todos: []
    });
    wrapper = mount(
      <Provider store={mockStore}>
        <TodoListContainer />
      </Provider>
    );
  });

  test("should exist", () => {
    expect(wrapper.exists()).toBe(true);
  });

  test("should render TodoList component", () => {
    expect(wrapper.find(TodoList)).toHaveLength(1);
  });
});
