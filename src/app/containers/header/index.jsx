import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import AddTodoForm from "app/components/addTodoForm";
import { addTodo, clearTodo } from "app/actions";
import { HeaderProto } from "app/prototypes";
import * as styles from "./styles.scss";

///////////////

/**
 * class respresenting Header Container
 *
 * @class
 */
class HeaderContainer extends Component {
  static propTypes = HeaderProto;

  /**
   * add a new todo in store via action call.
   */
  addTodo = text => {
    const { addTodo } = this.props.actions;

    addTodo(text);
  };

  render() {
    const { clearTodo } = this.props.actions;

    return (
      <div className={styles.header}>
        <AddTodoForm
          placeholder="Add your next task here!"
          onSave={this.addTodo}
        />
        <button id="clear" onClick={clearTodo}>
          Clear List
        </button>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ clearTodo, addTodo }, dispatch)
});

export default connect(
  null,
  mapDispatchToProps
)(HeaderContainer);
