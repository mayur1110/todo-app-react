import HeaderContainer from "./index";
import configureMockStore from "redux-mock-store";
import { Provider } from "react-redux";
import AddTodoForm from "app/components/addTodoForm";
import * as React from "react";
import { mount } from "enzyme";

describe("Header Container", () => {
  const createMockStore = configureMockStore();

  let mockStore;
  let wrapper;

  beforeEach(() => {
    mockStore = createMockStore({});
    wrapper = mount(
      <Provider store={mockStore}>
        <HeaderContainer />
      </Provider>
    );
  });

  test("should exist and have clear todo list button", () => {
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find("button#clear")).toHaveLength(1);
  });

  test("should render AddTodoForm component", () => {
    expect(wrapper.find(AddTodoForm)).toHaveLength(1);
  });
});
