import * as actions from "./index";

describe("todo actions", () => {
  test("addTodo should create ADD_TODO action", () => {
    expect(actions.addTodo("Test Todo")).toEqual({
      type: "ADD_TODO",
      text: "Test Todo"
    });
  });

  test("deleteTodo should create DELETE_TODO action", () => {
    expect(actions.deleteTodo(0)).toEqual({
      type: "DELETE_TODO",
      id: 0
    });
  });

  test("completeTodo should create COMPLETE_TODO action", () => {
    expect(actions.completeTodo(0)).toEqual({
      type: "COMPLETE_TODO",
      id: 0
    });
  });

  test("clearTodo should create CLEAR_TODO action", () => {
    expect(actions.clearTodo()).toEqual({
      type: "CLEAR_TODO"
    });
  });

  test("reArrangeTodo should create REORDER_TODO action", () => {
    expect(actions.reArrangeTodo(2, 0)).toEqual({
      type: "REORDER_TODO",
      from: 2,
      to: 0
    });
  });
});
