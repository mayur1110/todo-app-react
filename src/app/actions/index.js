/**
 * add todo action.
 *
 * @param {string} text todo text
 */
export const addTodo = text => ({ type: "ADD_TODO", text });

/**
 * delete todo action.
 *
 * @param {number} id todo item id
 */
export const deleteTodo = id => ({ type: "DELETE_TODO", id });

/**
 * complete todo toggle action.
 *
 * @param {number} id todo item id
 */
export const completeTodo = id => ({ type: "COMPLETE_TODO", id });

/**
 * clear todo list action.
 */
export const clearTodo = () => ({ type: "CLEAR_TODO" });

/**
 * rearrange todo list.
 *
 * @param {number} from index of item to be dragged
 * @param {number} to index where item to drop
 */
export const reArrangeTodo = (from, to) => ({ type: "REORDER_TODO", from, to });
