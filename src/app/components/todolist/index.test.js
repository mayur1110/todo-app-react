import TodoList from "./index";
import * as React from "react";
import { shallow } from "enzyme";
import DraggableList from "react-draggable";
import Todo from "app/components/todo";

describe("Todo component", () => {
  let wrapper;
  const props = {
    todos: [
      {
        text: "Task 1",
        completed: false,
        id: 0
      },
      {
        text: "Task 2",
        completed: false,
        id: 1
      },
      {
        text: "Task 3",
        completed: false,
        id: 3
      }
    ],
    deleteTodo: jest.fn(),
    reArrangeTodo: jest.fn(),
    completeTodo: jest.fn()
  };

  beforeEach(() => {
    wrapper = shallow(<TodoList {...props} />);
  });

  test("should exist and render DraggableList", () => {
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find(DraggableList)).toHaveLength(1);
  });

  test("should render two Todo components", () => {
    expect(wrapper.find(Todo)).toHaveLength(3);
  });

  test("should call action reArrangeTodo on order update with from/to index argument", () => {
    const draggableList = wrapper.find(DraggableList);

    draggableList.simulate("orderUpdate", 2, 0);

    expect(props.reArrangeTodo).toHaveBeenCalledWith(2, 0);
  });
});
