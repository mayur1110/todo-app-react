import React from "react";
import DraggableList from "react-draggable";

import Todo from "app/components/todo";
import { TodoListProto } from "app/prototypes";

///////////////

/**
 * class respresenting TodoList component
 *
 * @class
 */
export default class TodoList extends React.Component {
  static propTypes = TodoListProto;

  render() {
    const { deleteTodo, completeTodo, reArrangeTodo, todos } = this.props;

    const todoList = todos.map(item => {
      return (
        <Todo
          key={item.id}
          data={item}
          deleteTodo={deleteTodo}
          completeTodo={completeTodo}
        />
      );
    });

    return (
      <DraggableList onOrderUpdate={reArrangeTodo}>{todoList}</DraggableList>
    );
  }
}
