import React from "react";

import { TodoProto } from "app/prototypes";
import * as styles from "./styles.scss";

///////////////

/**
 * class respresenting Todo component
 *
 * @class
 */
export default class Todo extends React.Component {
  static propTypes = TodoProto;

  render() {
    const { data, deleteTodo, completeTodo } = this.props;

    return (
      <div className={styles.todo}>
        <label
          className={data.completed ? styles.completedTodo : styles.pendingTodo}
        >
          {data.text}
        </label>

        <button
          id="delete"
          className={styles.deleteButton}
          onClick={deleteTodo.bind(this, data.id)}
        >
          Delete
        </button>

        <button
          id="complete"
          className={styles.completeButton}
          onClick={completeTodo.bind(this, data.id)}
        >
          {data.completed ? "Mark as pending" : "Mark as completed"}
        </button>
      </div>
    );
  }
}
