import Todo from "./index";
import * as React from "react";
import { shallow } from "enzyme";

describe("Todo component", () => {
  let wrapper;
  const props = {
    data: {
      text: "Test Todo",
      completed: false
    },
    deleteTodo: jest.fn(),
    completeTodo: jest.fn()
  };

  beforeEach(() => {
    wrapper = shallow(<Todo {...props} />);
  });

  test("should exist and have text label, delete and complete todo buttons", () => {
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find("label")).toHaveLength(1);
    expect(wrapper.find("button#delete")).toHaveLength(1);
    expect(wrapper.find("button#complete")).toHaveLength(1);
  });

  test("should display todo item properly", () => {
    expect(wrapper.find("label").text()).toBe("Test Todo");
  });

  test("should call delete todo action on click of delete todo button", () => {
    wrapper.find("button#delete").simulate("click");

    expect(props.deleteTodo).toHaveBeenCalled();
  });

  test("should call complete todo action on click of complete todo button", () => {
    wrapper.find("button#complete").simulate("click");

    expect(props.completeTodo).toHaveBeenCalled();
  });
});
