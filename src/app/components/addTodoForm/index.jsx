import React, { Component } from "react";

import { AddTodoProto } from "app/prototypes";
import * as styles from "./styles.scss";

///////////////

/**
 * class respresenting AddTodoInput component
 *
 * @class
 */
export default class AddTodoForm extends Component {
  static propTypes = AddTodoProto;

  state = {
    todo: ""
  };

  /**
   * update state.todo value on input change.
   */
  handleChange = event => {
    this.setState({ todo: event.target.value });
  };

  /**
   * reset state.todo to blank string.
   */
  resetInput = () => {
    this.setState({ todo: "" });
  };

  /**
   * on submit form, save new todo and reset input field.
   */
  handleSubmit = event => {
    this.props.onSave(this.state.todo);
    this.resetInput();
    event.preventDefault();
  };

  render() {
    const { placeholder } = this.props;

    return (
      <form onSubmit={this.handleSubmit}>
        <input
          className={styles.input}
          type="text"
          placeholder={placeholder}
          value={this.state.todo}
          onChange={this.handleChange}
        />
        <button className={styles.submit} type="submit">
          Add todo
        </button>
      </form>
    );
  }
}
