import AddTodoForm from "./index";
import * as React from "react";
import { mount } from "enzyme";

describe("AddTodoForm component", () => {
  let wrapper;
  const props = {
    onSave: jest.fn(),
    placeholder: "Test Placeholder Text"
  };

  beforeEach(() => {
    wrapper = mount(<AddTodoForm {...props} />);
  });

  test("should exist and have add todo form", () => {
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find("form")).toHaveLength(1);
  });

  test("should change todo state value on input value change", () => {
    let input = wrapper.find("input");

    input.simulate("change", { target: { value: "Test Todo" } });

    expect(wrapper.state().todo).toBe("Test Todo");
  });

  test("should call onSave props method on form submit", () => {
    wrapper.find("form").simulate("submit");

    expect(props.onSave).toHaveBeenCalled();
  });

  test("should call onSave props method on form submit with proper input value", () => {
    let input = wrapper.find("input");
    let form = wrapper.find("form");

    input.instance().value = "Test Todo";
    input.simulate("change");
    form.simulate("submit");

    expect(props.onSave).toHaveBeenCalledWith("Test Todo");
  });
});
