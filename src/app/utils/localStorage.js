/**
 * save redux store on localstorage.
 *
 * @param {object} state redux object.
 */
export const setState = state => {
  try {
    localStorage.setItem("todoAppState", JSON.stringify(state));
  } catch (error) {
    // log error
  }
};

/**
 * serve store from local storage.
 */
export const getState = () => {
  try {
    const state = localStorage.getItem("todoAppState");

    return state === null ? undefined : JSON.parse(state);
  } catch (error) {
    return undefined;
  }
};
