import todos from "./todos";

describe("todos reducer", () => {
  test("should handle initial state", () => {
    expect(todos(undefined, {})).toEqual([
      {
        text: "list1",
        completed: false,
        id: 0
      }
    ]);
  });

  test("should handle ADD_TODO", () => {
    expect(
      todos([], {
        type: "ADD_TODO",
        text: "New todo item"
      })
    ).toEqual([
      {
        text: "New todo item",
        completed: false,
        id: 0
      }
    ]);
  });

  test("on ADD_TODO should add a new TODO on top of the list", () => {
    expect(
      todos(
        [
          {
            text: "Old todo item",
            completed: false,
            id: 0
          }
        ],
        {
          type: "ADD_TODO",
          text: "New todo item"
        }
      )
    ).toEqual([
      {
        text: "New todo item",
        completed: false,
        id: 1
      },
      {
        text: "Old todo item",
        completed: false,
        id: 0
      }
    ]);
  });

  test("should handle DELETE_TODO", () => {
    expect(
      todos(
        [
          {
            text: "Old todo item",
            completed: false,
            id: 0
          },
          {
            text: "About to delete todo item",
            completed: false,
            id: 1
          }
        ],
        {
          type: "DELETE_TODO",
          id: 1
        }
      )
    ).toEqual([
      {
        text: "Old todo item",
        completed: false,
        id: 0
      }
    ]);
  });

  test("should handle COMPLETE_TODO", () => {
    expect(
      todos(
        [
          {
            text: "About to complete todo item",
            completed: false,
            id: 1
          },
          {
            text: "Old todo item",
            completed: false,
            id: 0
          }
        ],
        {
          type: "COMPLETE_TODO",
          id: 1
        }
      )
    ).toEqual([
      {
        text: "About to complete todo item",
        completed: true,
        id: 1
      },
      {
        text: "Old todo item",
        completed: false,
        id: 0
      }
    ]);
  });

  test("should handle REORDER_TODO and rearrange todo list in proper order", () => {
    expect(
      todos(
        [
          {
            text: "Task 1",
            completed: false,
            id: 0
          },
          {
            text: "Task 2",
            completed: false,
            id: 1
          },
          {
            text: "Task 3",
            completed: false,
            id: 2
          }
        ],
        {
          type: "REORDER_TODO",
          from: 2,
          to: 0
        }
      )
    ).toEqual([
      {
        text: "Task 3",
        completed: false,
        id: 2
      },
      {
        text: "Task 1",
        completed: false,
        id: 0
      },
      {
        text: "Task 2",
        completed: false,
        id: 1
      }
    ]);
  });
});
