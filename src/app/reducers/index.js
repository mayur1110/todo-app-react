import { combineReducers } from "redux";

import todos from "./todos";

/**
 * combine reducers and export it.
 */
export default combineReducers({
  todos
});
