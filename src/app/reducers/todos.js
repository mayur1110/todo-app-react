const initialTodoList = [{ text: "list1", completed: false, id: 0 }];

/**
 * todo reducer
 *
 * @param {object} state redux state
 * @param {object} action action definition
 */
export default function todos(state = initialTodoList, action) {
  switch (action.type) {
    case "ADD_TODO":
      return [
        {
          text: action.text,
          completed: false,
          id: state.reduce((maxId, todo) => Math.max(todo.id, maxId), -1) + 1
        },
        ...state
      ];
    case "DELETE_TODO":
      return state.filter(todo => todo.id !== action.id);
    case "COMPLETE_TODO":
      return state.map(todo =>
        todo.id === action.id ? { ...todo, completed: !todo.completed } : todo
      );
    case "CLEAR_TODO":
      return [];
    case "REORDER_TODO":
      // eslint-disable-next-line no-case-declarations
      let reArrangedState = [...state];

      if (action.from < action.to) action.to--;

      // remove out dragged item[action.from] from array and put it back to the desired index [action.to]
      reArrangedState.splice(
        action.to,
        0,
        reArrangedState.splice(action.from, 1)[0]
      );
      return reArrangedState;
    default:
      return state;
  }
}
