import React from "react";

import TodoListContainer from "app/containers/todolist";
import HeaderContainer from "app/containers/header";

///////////////

// inline styling example.
const appStyle = {
  width: "500px",
  margin: "10px auto"
};

/**
 * class respresenting App
 *
 * @class
 */
export default class App extends React.Component {
  render() {
    return (
      <div style={appStyle}>
        <HeaderContainer />
        <TodoListContainer />
      </div>
    );
  }
}
