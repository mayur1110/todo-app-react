import PropTypes from "prop-types";

// Todo component prototype
export const TodoProto = {
  data: PropTypes.shape({
    text: PropTypes.string,
    completed: PropTypes.bool
  }),
  deleteTodo: PropTypes.func,
  completeTodo: PropTypes.func
};

// TodoList component prototype
export const TodoListProto = {
  todos: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string,
      completed: PropTypes.bool,
      priority: PropTypes.number
    })
  ),
  deleteTodo: PropTypes.func,
  reArrangeTodo: PropTypes.func,
  completeTodo: PropTypes.func
};

// AddTodo component prototype
export const AddTodoProto = {
  onSave: PropTypes.func,
  placeholder: PropTypes.string
};

// Header container prototype
export const HeaderProto = {
  actions: PropTypes.object
};

// Todo TodoList container prototype
export const TodoListContainerProto = {
  list: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string,
      completed: PropTypes.bool,
      priority: PropTypes.number
    })
  ),
  actions: PropTypes.object
};
