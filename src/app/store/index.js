import { composeWithDevTools } from "redux-devtools-extension";
import { createStore } from "redux";

import reducer from "app/reducers";
import { getState, setState } from "app/utils/localStorage";

/**
 * configure a store with devtool, subscribe it with localstorage update
 * and return store.
 */
export function configureStore() {
  const store = createStore(reducer, getState(), composeWithDevTools());
  store.subscribe(() => {
    setState(store.getState());
  });

  return store;
}
