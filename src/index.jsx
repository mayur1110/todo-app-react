import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import { configureStore } from "app/store";
import App from "app";

// import global css style file
import "./styles.scss";

///////////////

/**
 * render react application on Element "app"
 */
ReactDOM.render(
  <Provider store={configureStore()}>
    <App />
  </Provider>,
  document.getElementById("app")
);
